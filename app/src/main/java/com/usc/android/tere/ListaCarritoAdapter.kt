package com.usc.android.tere

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_itemcarrito.view.*
import tere.Helpers
import tere.Producto
import tere.Tere

/**
 * Created by Usuario on 13/01/2018.
 */


class ListaCarritoAdapter(
        var listItems: ArrayList<Producto>, var panel: Panel) : RecyclerView.Adapter<ListaCarritoAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(listItems[position])
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_itemcarrito, parent, false)

        return ViewHolder(v, panel,this)
        //To change body of created functions use File | Settings | File Templates.
    }


    class ViewHolder(itemView: View, var panel: Panel, var cla: ListaCarritoAdapter) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(producto: Producto) {
            itemView.nombre.text=producto.nombre
            itemView.descripcion.text = "\$ ${producto.getPrecioUnitario()} - Total: \$ ${producto.getPrecioTotal()}"
            itemView.cantidad.text = producto.cantidad.toString()
            itemView.cantidad.visibility = View.VISIBLE
            //itemView.iv_name.setImageResource(user.image)
            Glide.with(itemView.image.getContext())
                    .load(producto.getImagen("b")).thumbnail(Glide.with(itemView.context).load(R.drawable.loading2))
                    .centerCrop()
                    .into(itemView.image)

            itemView.cardView.setOnClickListener(View.OnClickListener { mostrarInfo(producto) })
        }

        private fun mostrarInfo(producto: Producto) {
            Helpers.alertVerMas(itemView.context,producto, panel,{
                cla.notifyItemRemoved(getAdapterPosition())
                cla.notifyItemRangeChanged(getAdapterPosition(), Tere.productos.size)
            }, {
                cla.notifyItemRangeChanged(getAdapterPosition(), Tere.productos.size)
            })
        }
    }
}
