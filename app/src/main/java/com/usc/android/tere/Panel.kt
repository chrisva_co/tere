package com.usc.android.tere

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_panel.*
import kotlinx.android.synthetic.main.fragment_itemcarrito.*
import tere.Helpers
import tere.Tere
import java.text.DecimalFormat
import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.zxing.Result
import com.kosalgeek.genasync12.AsyncResponse
import com.kosalgeek.genasync12.PostResponseAsyncTask
import kotlinx.android.synthetic.main.fragment_buscar.*
import kotlinx.android.synthetic.main.fragment_pago.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import tere.Producto
import java.lang.Double.parseDouble
import java.util.*
import android.content.SharedPreferences
import tere.Auxiliar
import tere.Helpers.alert
import tere.Helpers.isNumeric


class Panel : AppCompatActivity() {

    val bf = BusquedaFragment()
    val pf = PagoFragment()
    val msg="Lista de productos"
    var listaCarritoA: ListaCarritoAdapter? = null

    object st{
        var sharedPref: SharedPreferences? = null
        val url="http://tere.paginaweb2.com"

        fun monitorearPago(context:Context, facturaId:String="", panel:Panel?=null){

            var fid = facturaId

            if (fid.isNullOrBlank())
                fid = Panel.st.sharedPref!!.getString(context.getString(R.string.facturaId), "")

            //si no hay servicio iniciado, y hay factura que monitorear
            if (!Helpers.servicioIniciado() && !fid.isNullOrBlank()) {
                //Comienzo monitoreo
                Helpers.servicioStart(2000) { count ->
                    Helpers.getUrlContents(context, "http://tere.paginaweb2.com/factura/$fid/pagado", 3, fun(r) {
                        println("CT: RES"+r)

                        if (r=="1"){
                            //paro el servicio
                            Helpers.servicioDetener()
                            //eliminar Sharedreferences
                            st.sharedPref!!.edit().remove(context.getString(R.string.facturaId)).commit()

                            try {
                                //si estoy dentro de la app
                                Helpers.alert(context, "Pago realizado",
                                        "Factura pagada satisfactoriamente", "Ver factura") {
                                    panel!!.mostrarPanelProcesarPago("http://tere.paginaweb2.com/factura/" + fid+"?m=1", "Pago realizado", "facturas")
                                }

                                val intent = Intent(context, Auxiliar::class.java).putExtra("facturaId", fid)
                                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)

                                Helpers.sendNotification(context,1,context.getString(R.string.title_activity_panel), "Tu pago ha sido realizado",
                                        R.drawable.tereicon2, intent)
                                panel!!.borrarLista()
                            }catch (e:Exception){

                            }
                        }

                    }, null)
                    println("CT: " + count + " fi: "+fid)

                }
            }else{
                println("CT: Hay un servicio iniciado previamente")
            }
        }
    }

    private fun bloqueBuscarProducto(){
        bf.panel=this
        setTitle("Buscar un producto")
        supportFragmentManager.beginTransaction()
                .replace(R.id.contenedorFragment, bf)
                .commit()
        mostrarOcultarLista(View.INVISIBLE)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        actualizarCantidades()

        when (item.itemId) {
            R.id.navigation_buscar -> {
                bf.recomendaciones=false
                bloqueBuscarProducto()
                return@OnNavigationItemSelectedListener true
            }


            R.id.navigation_escanear -> {
                if (Tere.numeroTelefono.isNullOrBlank()){
                    cambiarTelefono {
                        escanear()
                    }
                    return@OnNavigationItemSelectedListener false
                }
                if (title==msg)
                    escanear()
                setTitle(msg)
                mostrarOcultarLista(View.VISIBLE)
                //item.title = "Escanear"

                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_pagar -> {
                if (Tere.cantidadProductos==0){
                    Helpers.pasarTextoAVoz(applicationContext,
                            "Antes de pagar, primero añade un producto a la lista")
                    alert("Antes de pagar, primero añade un producto a la lista", "Tere"){
                        yesButton {}
                    }.show()
                    return@OnNavigationItemSelectedListener false
                }

                recomendarProductos()


                mostrarPanelProcesarPago("http://tere.paginaweb2.com/app/login",
                                        "Procesar pago",
                                        Tere.listaProductosXComa)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun recomendarProductos(){
        buscarProductosPorIds(Tere.listaProductosXComa, 5){
            var msg="Productos que no estan en la lista: \n"
            it.forEach {
                msg+="\n"+it.nombre+":  $"+DecimalFormat("###,###.##").format(it.valorUnitario)
            }

            Helpers.pasarTextoAVoz(applicationContext,"Quizás te interece algunos de estos productos")

            alert( msg.trim(), "Recomendaciones"){
                positiveButton("Ir a pagar") { }
                negativeButton("Regresar a la lista") {
                   mostrarOcultarLista(View.VISIBLE)
                }
            }.show()
        }


    }

    fun mostrarPanelProcesarPago(url:String, titulo:String, extra:String, zoomY100:Boolean=false){
        //primera vez, usa estas variables
        PagoFragment.vars.url = url
        PagoFragment.vars.extra = extra
        PagoFragment.vars.zoomY100 = zoomY100


        setTitle(titulo)
        mostrarOcultarLista(View.INVISIBLE)


        try{

            if (zoomY100)
                pf.webPagar.setInitialScale(100)
            else
                pf.webPagar.setInitialScale(200)

            pf.webPagar.settings.setUserAgentString(generarExtra(extra))
            pf.webPagar.loadUrl(url)

        }catch (e:Exception){}

        supportFragmentManager.beginTransaction()
                .replace(R.id.contenedorFragment, pf)
                .commit()
    }

    fun actualizarCantidades() {
       Tere.actualizarCuentasTotales()
        if (Tere.cantidadProductos!=0) {
            cantidadProductos.text = "Cantidad: ${Tere.cantidadProductos}"
            cantidadDinero.text = "Total: \$${DecimalFormat("###,###.##").format(Tere.cantidadPrecio)}"
        }
        else{
            cantidadProductos.text=""
            cantidadDinero.text=""
        }
    }


    fun mostrarOcultarLista(visibilidad: Int){
        containerRecycler.visibility = visibilidad
    }



    fun buscarProductosPorIds(ids:String, cantidadResultados:Int, fn:(x:ArrayList<Producto>)->Any){

        try {
            val url2 = "http://tere.paginaweb2.com/app/recomendar/"+ids+"/"+cantidadResultados

            val postData = HashMap<String,String>()
            postData.put("ids", ids)

            val task2 = PostResponseAsyncTask(this, postData, AsyncResponse { s ->
                try {

                    Tere.llenarArregloResultados(s)

                    if (Tere.resultadosBusqueda.size !=0){
                        fn(Tere.resultadosBusqueda)
                    }
                    //println(Tere.resultadosBusqueda)
                    //Toast.makeText(this, ""+Tere.resultadosBusqueda, Toast.LENGTH_LONG).show()
                } catch (e: Exception) {
                    println(e)
                    e.printStackTrace()
                    //Toast.makeText(this, "Servidor no disponible", Toast.LENGTH_LONG).show()
                }
            })
            task2.execute(url2)
        } catch (e: Exception) {
           // Toast.makeText(this, "Servidor no existe: "+e.message, Toast.LENGTH_LONG).show();
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_panel)
        title = msg

        //creo el mediaplayer
        Helpers.music = MediaPlayer.create(applicationContext,R.raw.pip_escaneado_1)
        st.sharedPref = applicationContext.getSharedPreferences("referenciasTere", Context.MODE_PRIVATE);


        st.monitorearPago(this,"",this)

        //Busco si tiene el telefono guardado en el sharedpreferences
        Tere.numeroTelefono = st.sharedPref!!.getString(getString(R.string.numTel), "")
        //toast(sharedTelefono)

        if (Tere.numeroTelefono.isNullOrBlank()) {
           cambiarTelefono(){
               Helpers.pasarTextoAVoz(applicationContext,getString(R.string.txt_como_escanear))
               animarBotonEscaner()
           }
        }
        //"Añade un producto a la lista escaneando su código de barras."
        else if (Tere.productos.size==0)
            Helpers.pasarTextoAVoz(applicationContext,getString(R.string.txt_como_escanear))

//        setSupportActionBar(toolbar)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.menu.findItem(R.id.navigation_escanear).setChecked(true)


        listaCarritoA = ListaCarritoAdapter(Tere.productos, this) //context
        listaCarrito.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        listaCarrito.adapter = listaCarritoA
    }

    fun animarBotonEscaner(){

    }

    fun refrescarVista() {
        //Mensaje de lista Limpia
        if (Tere.productos.size == 0)
            mListaLimpia.visibility=View.VISIBLE
        else
            mListaLimpia.visibility=View.GONE

        try {
            listaCarritoA!!.notifyDataSetChanged()
            listaCarrito.scrollToPosition(Tere.productos.size - 1)
            actualizarCantidades()
        }catch (e:Exception){toast("Lista vacia")}
    }

    fun cambiarTelefono(fn:()->Any){
        Helpers.prompt(this, "Antes de continuar",
                "Primero ingresa el número de tu teléfono movil", null, false) { dato ->

            var editor = st.sharedPref!!.edit()
            Tere.numeroTelefono = dato
            editor.putString(getString(R.string.numTel), dato).commit()
            fn()
        }
    }



    fun escanear() {
        var activityPanel = Intent(applicationContext, ScannerActivity::class.java)
        startActivity(activityPanel)
    }

    override fun onResume() {
        super.onResume()

        if (Tere.ultimoEscaneado.isEmpty()) {
            refrescarVista()
            return
        }

       //pip

        //Compruebo que sea un codigo numerico, si esta vacio retorna.
        if (!Tere.ultimoEscaneado.isNumeric()){
            toast("Codigo incorrecto") //si no lo es, envio mensaje de error y muero aqui.
            return
        }

        var productoIndex = Tere.buscarProductoIdBarCode(Tere.ultimoEscaneado)

        //si esta, le sumo +1 al elemento que lo tenga.
        if (productoIndex!=-1){
            Tere.productos[productoIndex].cantidad++

            refrescarVista()
        }else{
            buscarProductoEnInternet(Tere.ultimoEscaneado)
        }
        Tere.ultimoEscaneado=""
    }

    fun buscarProductoEnInternet(barCode:String){
        //println(barCode)
        try {
            val url2 = "http://tere.paginaweb2.com/app/escanear"
            var postData = HashMap<String,String>()
            postData.put("barCode", barCode)

            var task2 = PostResponseAsyncTask(this, postData, AsyncResponse { s ->
                try {
                    println(s)
                    var gson2 = GsonBuilder().create()
                    var producto:Producto = gson2.fromJson<Producto>(s, Producto::class.java)

                    producto.cantidad=1
                    println(producto)
                    //Agregarlo a la lista
                    Tere.productos.add(producto)
                    refrescarVista()

                } catch (e: Exception) {
                    toast("Producto no registrado en el almacén")
                }
            })
            task2.execute(url2)
        } catch (e: Exception) {
            println(e)
            toast("Servidor no disponible ")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_panel, menu)
        return true
    }

    fun borrarLista(){
        setTitle(msg)
        mostrarOcultarLista(View.VISIBLE)
        Tere.productos.clear()
        listaCarritoA!!.notifyDataSetChanged()
        actualizarCantidades()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_vaciar_l -> {
                alert("Estas seguro?", "Estas a punto de vaciar la lista") {
                    yesButton {
                        borrarLista()
                    }
                    noButton {}
                }.show()
                return true
            }
            R.id.action_sobre_nosotros -> {
                alert("Con tere podras pagar tus productos en supermercados sin necesidad de hacer largas filas.  " +
                        "\n\nDesarrollado por:\nChristian Valencia " +
                        "\nPaula Salazar" +
                        "\n\nCon ayuda de:" +
                        "\nClaudia Zúñiga \n\nUniversidad santiago de Cali\nComba I+D",
                        "Sobre Tere") {
                    yesButton {}
                }.show()


                return true
            }
            R.id.list_item_from_bar -> {
                setTitle("Lista de productos")
                item.title = "Escanear"
                mostrarOcultarLista(View.VISIBLE)
                actualizarCantidades()
                return true
            }
            R.id.action_facturas -> {
                mostrarPanelProcesarPago("http://tere.paginaweb2.com/facturas","Mis facturas", "facturas")
                return true
            }
            R.id.action_mapa -> {
                mostrarPanelProcesarPago("http://tere.paginaweb2.com/ubicacion","Mapa", "facturas", true)
                return true
            }

            R.id.action_salir -> {
                borrarLista()
                cambiarTelefono(){}
                //eliminar monitoreos
                //paro el servicio
                Helpers.servicioDetener()
                st.sharedPref!!.edit().remove(getString(R.string.facturaId)).commit()//eliminar Sharedreferences

                return true
            }



        }
        return super.onOptionsItemSelected(item)
    }

    fun prueba(){
        Toast.makeText(this, "Prueba desde Panel", Toast.LENGTH_LONG).show()
    }
    override fun onPause() {
        super.onPause()
        Tere.ultimoEscaneado=""
    }






}

