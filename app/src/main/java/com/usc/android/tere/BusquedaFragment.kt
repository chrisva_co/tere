package com.usc.android.tere


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.kosalgeek.genasync12.AsyncResponse
import com.kosalgeek.genasync12.PostResponseAsyncTask
import kotlinx.android.synthetic.main.activity_panel.*
import kotlinx.android.synthetic.main.fragment_buscar.*
import kotlinx.android.synthetic.main.fragment_buscar.view.*
import tere.Helpers
import tere.Tere


/**
 * A fragment with a Google +1 button.
 */
class BusquedaFragment: Fragment() {

    lateinit var panel: Panel
    var recomendaciones: Boolean = false //inactivo

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_buscar, container, false)


        view.btnBuscar.setOnClickListener(View.OnClickListener { buscarProducto() })
        return view
    }



    fun buscarProducto(){

        if ((this.textoABuscar.text.toString()=="Ver recomendaciones" || recomendaciones) && Tere.resultadosBusqueda.size != 0){
            actualizarListaBusqueda()
            return
        }
        try {
            val url2 = "http://tere.paginaweb2.com/app/buscar"

            val postData = HashMap<String,String>()
            postData.put("valor", this.textoABuscar.text.toString())
            postData.put("fila", "nombre")
            postData.put("first", "0")

            val task2 = PostResponseAsyncTask(context, postData, AsyncResponse { s ->
                try {
                   // val dato = Helpers.stringAJson(s)
                    //lleno el arreglo de arrayResultados
                    Tere.llenarArregloResultados(s)
                    println(Tere.resultadosBusqueda)

                    actualizarListaBusqueda()
                   // this.lbResultado.text = s
                    //ExplorArt.llenar_ListasDeObjetos_JsonToArray(dato.get("Resultados").toString())

                } catch (e: Exception) {
                    println(e)
                    e.printStackTrace()
                    Toast.makeText(context, "Servidor no disponible", Toast.LENGTH_LONG).show()
                }

                //Toast.makeText(Login.this,ExplorArt.grupos.get(1).nombre+" |id:"+ExplorArt.grupos.get(1).id, Toast.LENGTH_LONG).show();
            })

            task2.execute(url2)
        } catch (e: Exception) {
            // Toast.makeText(this, "Servidor no existe", Toast.LENGTH_LONG).show();
        }
    }

    fun actualizarListaBusqueda(){

        var view = this
        var listaBusquedaA = ListaBusquedaAdapter(Tere.resultadosBusqueda, panel, this) //context
        view.listaResultados.layoutManager = LinearLayoutManager(view.context, LinearLayout.VERTICAL, false)

        view.listaResultados.adapter = listaBusquedaA
    }
}
