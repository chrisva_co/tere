package com.usc.android.tere


import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import kotlinx.android.synthetic.main.fragment_pago.view.*
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.widget.Toast
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.util.Log
import kotlinx.android.synthetic.main.fragment_pago.*
import tere.Helpers
import tere.Tere
import android.webkit.ConsoleMessage




/**
 * A simple [Fragment] subclass.
 */
fun generarExtra(extra:String) = ";;;"+extra+";;;"+Tere.numeroTelefono

class PagoFragment : Fragment() {

    object vars{
        var zoomY100 = false
        var url = "" //http://tere.paginaweb2.com/app/login
        var extra = "" //ids y cantidad o facturas
        val ua="Mozilla/5.0 (Linux; Android 5.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Mobile Safari/537.36 "
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_pago, container, false)

        var webSettings = view.webPagar.settings;

        view.webPagar.settings.setBuiltInZoomControls(vars.zoomY100)
        view.webPagar.settings.setDisplayZoomControls(false)
        if (vars.zoomY100)
            view.webPagar.setInitialScale(100)
        else
            view.webPagar.setInitialScale(200)

        webSettings.setAppCacheEnabled(true)
        webSettings.setJavaScriptEnabled(true)
        webSettings.setLoadWithOverviewMode(true)
        webSettings.setAllowFileAccess(true)
        webSettings.setUserAgentString(vars.ua+generarExtra(vars.extra)) //webSettings.userAgentString

        webSettings.setDomStorageEnabled(true);

        view.webPagar.setWebViewClient(myWebViewClient())

        view.webPagar.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                try{
                    progressBar.setProgress(progress)
                    if (progress == 100) {
                        progressBar.setVisibility(View.GONE)

                        if (view.url.contains("/factura/"))
                            comenzarMonitoreoDeFactura(view.url, activity as Panel)

                    }else
                        progressBar.setVisibility(View.VISIBLE)
                }catch (e: Exception){
                    //progressBar.setVisibility(View.GONE)
                }
            }

            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
                Log.d("MyApplication Chrome", consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId())
                return super.onConsoleMessage(consoleMessage)
            }
            //Para subir archivos, upload https://github.com/coomar2841/WebViewUploadFile/blob/master/app/src/main/java/com/kbeanie/webviewuploadfile/MainActivity.java
        }

        //if SDK version is greater of 19 then activate hardware acceleration otherwise activate software acceleration
        if (Build.VERSION.SDK_INT >= 19) {
            view.webPagar.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 19) {
            view.webPagar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        view.webPagar.loadUrl(PagoFragment.vars.url);
        //https://clinishop.co/subir_imagen.php
        return view
    }

    //este metodo funciona siempre y cuando el url sea /factura/
    fun comenzarMonitoreoDeFactura(url:String, panel:Panel){
        var facturaId = Panel.st.sharedPref!!.getString(getString(R.string.facturaId), "")

        //Si no hay monitoreo de factura
        if (!Helpers.servicioIniciado()) {
            Helpers.getUrlContents(context, url.replace("?m=1", "").split("#")[0] + "/pagado", 3, {

                r ->
                if (r != "1") { //si es una factura sin pagar
                    if (facturaId.isNullOrBlank()) {
                        //extraigo factura
                        facturaId = url.split("factura/")[1].replace("?m=1", "").split("#")[0]
                        println("id Factura: " + facturaId)
                        //guardo factura a monitorear
                        Panel.st.sharedPref!!.edit().putString(getString(R.string.facturaId), facturaId).commit()
                    }
                    Panel.st.monitorearPago(context, facturaId, panel)
                }
            })
        }else{
            println("CT: WebView- servicio previamente iniciado")
        }
    }


    inner class myWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }
}// Required empty public constructor
