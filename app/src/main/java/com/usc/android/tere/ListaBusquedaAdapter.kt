package com.usc.android.tere

import android.opengl.Visibility
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_itemcarrito.view.*
import tere.Helpers
import tere.Producto

/**
 * Created by Usuario on 13/01/2018.
 */


class ListaBusquedaAdapter(
        var listItems: ArrayList<Producto>, var panel: Panel, var busquedaFragment: BusquedaFragment) : RecyclerView.Adapter<ListaBusquedaAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(listItems[position])
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_itemcarrito, parent, false)
        return ViewHolder(v, panel)
        //To change body of created functions use File | Settings | File Templates.
    }


    class ViewHolder(itemView: View, var panel: Panel) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(producto: Producto) {
            var nombreProducto = producto.nombre.toLowerCase().capitalize()
            val max = 25
            if (nombreProducto.length>max){
                nombreProducto = nombreProducto.substring(0, max).trim()+"..."
            }
            itemView.nombre.text=nombreProducto
            itemView.descripcion.text = "\$ ${producto.getPrecioUnitario()}"
            itemView.cantidad.text = ""
            itemView.cantidad.visibility = View.INVISIBLE
            itemView.iconUbicacion.visibility = View.VISIBLE
            //itemView.iv_name.setImageResource(user.image)
            Glide.with(itemView.image.getContext())
                    .load(producto.getImagen("b")).thumbnail(Glide.with(itemView.context).load(R.drawable.loading2))
                    .centerCrop()
                    .into(itemView.image)

            itemView.cardView.setOnClickListener(View.OnClickListener { mostrarInfo(producto) })
        }

        private fun mostrarInfo(producto: Producto) {
            panel.mostrarPanelProcesarPago("http://tere.paginaweb2.com/ubicacion/"+producto.categorias,"Buscar un producto", "ubicacion", true)
            //Helpers.alert(itemView.context, producto.nombre, producto.categorias,"Cerrar")

            /*  Helpers.alertVerMas(itemView.context,
                      producto.nombre,
                      "\$ ${producto.getPrecioUnitario()} - Total: \$ ${producto.getPrecioTotal()}",
                      producto.getImagen(),
                      producto.cantidad.toString(), panel) */
        }
    }
}
