package com.usc.android.tere

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.beardedhen.androidbootstrap.TypefaceProvider
import org.jetbrains.anko.toast
import tere.Tere

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        TypefaceProvider.registerDefaultIconSets();

    }


    fun entrarAlPanel(view: View){
        var activityPanel = Intent(applicationContext, Panel::class.java)
        startActivity(activityPanel)

        Tere.init()
    }
}
