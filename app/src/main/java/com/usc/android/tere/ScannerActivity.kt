package com.usc.android.tere

import me.dm7.barcodescanner.zxing.ZXingScannerView
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.zxing.Result
import tere.Helpers
import tere.Tere.ultimoEscaneado
import android.os.PowerManager
import android.view.WindowManager


/**
 * Created by Usuario on 31/01/2018.
 */
class ScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)                // Set the scanner view as the content view
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        //ultimoEscaneado="333444"
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    override fun handleResult(rawResult: Result) {
       //println("RESULT: " +rawResult.getBarcodeFormat().toString()) // Prints the scan format (qrcode, pdf417 etc.)

        ultimoEscaneado = rawResult.getText()

        //pip
        Helpers.reproducirRAW(applicationContext,R.raw.pip_escaneado_1)

       // mScannerView!!.resumeCameraPreview(this)
       // Helpers.alert(this,"Titulo","mensaje en camera")
       onBackPressed()
    }
}