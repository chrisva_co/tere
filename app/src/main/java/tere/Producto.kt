package tere

import java.text.DecimalFormat

/**
 * Created by Usuario on 13/01/2018.
 */
data class Producto(val id: Int,
                    var nombre: String,
                    var descripcion: String,
                    var barCode: String /*Barcode id*/,
                    var cantidad: Int = 1,
                    var image: String,
                    var valorUnitario: Int,
                    var categorias: String = "",
                    var formato: String = "Unidad", //unidad o peso
                    var ubicacion: String = "",
                    var posX: Int = -1,
                    var posY: Int = -1,
                    //falta los height
                    var marca: String = "",
                    var conIva: Int = 1,
                    var tipo: String = "",
                    var extra: String = ""
){

    fun getImagen(sizeImgur:String = "l"): String {
        if (image.contains("imgur") and !image.contains(".png"))
            return image+sizeImgur+".png"
        return image
    }

    fun aumentarCantidad(){
        cantidad++
    }

    fun disminuirCantidad(){
        if (cantidad > 1)
            cantidad--
    }

    fun getPrecioTotal(formatear: Boolean=false):String{
        if (formatear)
           return (valorUnitario*cantidad).toString()
        return DecimalFormat("###,###.##").format(valorUnitario*cantidad)
    }

    fun getPrecioUnitario(): String {
        var formato = DecimalFormat("###,###.##")
        return formato.format(valorUnitario)
    }
}