package tere

/*REQUIERE VOLLEY Y GLIDE*/

import android.app.Activity
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.support.v7.app.AlertDialog
import android.support.v7.app.NotificationCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.usc.android.tere.Panel
import com.usc.android.tere.R
import kotlinx.android.synthetic.main.alert_ver_mas_detalles.view.*
import kotlinx.android.synthetic.main.prompt.view.*
import org.json.JSONObject
import java.lang.Double
import java.util.*
import kotlin.concurrent.timer

/**
 * Created by Usuario on 18/01/2018.
 */
object Helpers {

    lateinit var textoAvoz:TextToSpeech
    lateinit var music: MediaPlayer
    private var detenerServicio = true
    private var timerEjecuciones = 0


    fun stringAJson(json: String): JSONObject? {
        try {
            return JSONObject(json)
        } catch (t: Throwable) {
            Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"")
        }
        return null
    }

    //Especificamente de ExplorArt
    fun alert(context: Context, titulo: String, msg: String, nombreBoton:String="Ok", fn:(()->Any)?=null) {
        AlertDialog.Builder(context)
                .setIcon(R.drawable.ic_shopping_basket)
                .setTitle(titulo)
                .setMessage(msg)
                .setPositiveButton(nombreBoton, DialogInterface.OnClickListener { dialog, which ->
                    fn?.let { fn() }
                   // fn?.invoke()
                }).show()
    }




    fun alertVerMas(context: Context, producto:Producto, panel: Panel, fnq:()->Any, fnc:()->Any) {
        var imageUrl = producto.getImagen()

        val view = LayoutInflater.from(context).inflate(R.layout.alert_ver_mas_detalles, null)
        view.aTitulo.text = producto.nombre
        view.aDescripcion.text = producto.descripcion
        view.cantidad.text = producto.cantidad.toString()

        if (imageUrl.contains("imgur.")) {
            imageUrl = imageUrl + ".png"
        }
        Glide.with(view.context).load(imageUrl).centerCrop().fitCenter()
                .thumbnail(Glide.with(view.context).load(R.drawable.loading2))
                .into(view.aImage)

        view.aExtra.text = "Importe: \$${producto.getPrecioTotal()}"

        view.btnAumentarC.setOnClickListener {
            producto.aumentarCantidad()
            view.cantidad.text = producto.cantidad.toString()
            view.aExtra.text = "Importe: \$${producto.getPrecioTotal()}"

        }

        view.btnDisminuirC.setOnClickListener {
            producto.disminuirCantidad()
            view.cantidad.text = producto.cantidad.toString()
            view.aExtra.text = "Importe: \$${producto.getPrecioTotal()}"

        }


        val alertadd = AlertDialog.Builder(context)
        alertadd.setView(view)

        alertadd.setNegativeButton("Quitar de lista", DialogInterface.OnClickListener( { dialog, which ->

            Tere.productos.remove(producto)
            fnq()
            panel.actualizarCantidades()
        }))

        alertadd.setPositiveButton("Cerrar", DialogInterface.OnClickListener( { dialog, which ->
            fnc()
            panel.actualizarCantidades()
        }))

       alertadd.show()
    }






    fun prompt(context:Context, titulo: String, desc:String, image:String?, btnCancelar:Boolean = true, fn:(dato:String)->Any){

        val view = LayoutInflater.from(context).inflate(R.layout.prompt, null)
        view.pTitulo.text = titulo
        view.pDescripcion.text = desc


        if (!image.isNullOrBlank()) {
            var imageUrl = image
            if (imageUrl!!.contains("imgur."))
                imageUrl = image + ".png"

            view.pImage.visibility = View.VISIBLE

            Glide.with(view.context).load(imageUrl).centerCrop().fitCenter()
                .thumbnail(Glide.with(view.context).load(R.drawable.loading2))
                .into(view.pImage)
        }

        val alertp = AlertDialog.Builder(context)
        alertp.setView(view)

        if (btnCancelar)
            alertp.setNegativeButton("Cerrar", DialogInterface.OnClickListener( { dialog, which ->

            }))

        alertp.setPositiveButton("OK", DialogInterface.OnClickListener( { dialog, which ->
            fn(view.pEditText.text.toString())
        }))

        alertp.show()
    }







    fun pasarTextoAVoz(context:Context, convertTextToSpeech: String) {

        textoAvoz = TextToSpeech(context, TextToSpeech.OnInitListener { status ->
            if (status == TextToSpeech.SUCCESS) {
                //textoAvoz.setLanguage(new Locale("en", "US"));
                textoAvoz.setLanguage(Locale("spa", "MEX"))

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    textoAvoz.speak(convertTextToSpeech, TextToSpeech.QUEUE_FLUSH, null, null)
                } else {
                    textoAvoz.speak(convertTextToSpeech, TextToSpeech.QUEUE_FLUSH, null)
                }
            } else if (status == TextToSpeech.ERROR) {
                Toast.makeText(context, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun reproducirRAW(context:Context,archivo: Int) {
        //in activityMain: Helpers.music = MediaPlayer.create(applicationContext,R.raw.pip_escaneado_1)
        music.release()
        music = MediaPlayer.create(context, archivo)
        music.setVolume(0.2f, 0.2f)
        music.start()
    }



    fun getUrlContents(context:Context, url:String, maxCaracter:Int=500, fn:(x:String)->Any, fne: ((y: String) -> Any)? =null){
        //var pb = ProgressDialog.show(context, null, "loading...", false) pb.hide() //;)
        var stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    if (response.length>maxCaracter)
                        fn(response.substring(0, maxCaracter))
                    fn(response.toString())
                },
                Response.ErrorListener { error ->
                    fne?.let{ error.toString() }
                })
        Volley.newRequestQueue(context).add(stringRequest)
    }



    fun sendNotification(context:Context, index:Int, titulo:String, desc:String, icon:Int, intent: Intent) {
        val builder = NotificationCompat.Builder(context).setSmallIcon(icon).setContentTitle(titulo).setContentText(desc)

        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        builder.setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
        //Opciones adiccionales
        builder.setAutoCancel(true).setLights(Color.BLUE, 500, 500).setVibrate(longArrayOf(500, 500))

        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(index, builder.build())
    }

    //no detiene los servicios que ya se han comenzado
    fun servicioStart(ejecutarCadaSegundos:Long, fn:(te:Int)->Any){
        detenerServicio = false
        timerEjecuciones = 0

        if (!detenerServicio)
            Timer().schedule(object: TimerTask(){
                override fun run() {
                    fn(timerEjecuciones++)

                    //println("timer: ${vars.i++}")
                    if (detenerServicio) cancel()
                }
            },1,ejecutarCadaSegundos)
    }

    fun servicioDetener(){
        timerEjecuciones = 0
        detenerServicio = true
    }

    fun servicioIniciado():Boolean{
        return timerEjecuciones!=0
    }

    fun String.isNumeric() =
            try {
                var num = Double.parseDouble(this.toString())
                true
            }catch (e: Exception){
                false
            }
}


//se habre y regresa
class Auxiliar : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finish()
    }
}