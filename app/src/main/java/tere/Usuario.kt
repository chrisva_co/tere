package tere

/**
 * Created by Usuario on 13/01/2018.
 */
data class Usuario(var id: Int,
              var nombre: String,
              var direccion: String,
              var telefono: String,
              var email: String,
              var ubicacion: String,
              var avatar: String )