package tere

import com.google.gson.GsonBuilder
import org.jetbrains.anko.collections.forEachByIndex
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Usuario on 18/01/2018.
 */
object Tere{

    val productos = ArrayList<Producto>()
    var resultadosBusqueda = ArrayList<Producto>()
    var cantidadProductos = 0
    var cantidadPrecio = 0

    var ultimoEscaneado = ""
    var listaProductosXComa = "" //1:3,5:2,2:7,
    var numeroTelefono = ""
    fun init () {
        /*productos.add(Producto(2,"Leche arquería entera", "", 123, 3, "https://imgur.com/3az5Qww", 15000))
        productos.add(Producto(3, "Azucar Mauelita refinada", "", 456, 2, "https://imgur.com/kbaPxB7", 12000))
        productos.add(Producto(1,"Pan Bimbo", "", 789, 4, "https://imgur.com/joNamSo",6000))*/
        //actualizarCuentasTotales()
    }


    fun actualizarCuentasTotales(){
        cantidadProductos = 0
        cantidadPrecio = 0

        listaProductosXComa = ""
        productos.forEach {
            if (it.cantidad == 0)
                it.cantidad = 1
            cantidadProductos+=it.cantidad
            cantidadPrecio+=(it.valorUnitario*it.cantidad)

            listaProductosXComa+=""+it.id+":"+it.cantidad+","
        }
    }

    fun llenarArregloResultados(jsonResultados: String):ArrayList<Producto>{
        var gson2 = GsonBuilder().create()
        var arregloObjeto = gson2.fromJson<Array<Producto>>(jsonResultados, Array<Producto>::class.java)
        resultadosBusqueda = ArrayList<Producto>(Arrays.asList<Producto>(*arregloObjeto))
        return resultadosBusqueda
    }

    fun agregarProducto(){

    }

    fun eliminarProducto(id: Int){

    }

    fun buscarProductoIdBarCode(q: String):Int{//barCode
        if (q.isNullOrBlank())
            return -1
        try {
            productos.forEachIndexed { index, producto ->
                if (producto.barCode == q )
                    return index
            }
        }catch (e:Exception){
            return -1
        }
        return -1
    }

}